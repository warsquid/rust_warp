use image;
use image::io::Reader as ImageReader;
use image::RgbaImage;
use noise::{NoiseFn, Perlin};
use std::f64::consts::PI;
use webp_animation::{Encoder, EncoderOptions, EncodingConfig};

fn main() {
    let img = ImageReader::open("bismuth.jpg")
        .expect("Failed to load image")
        .decode()
        .expect("Failed to decode")
        .into_rgba8();

    let (width, height) = img.dimensions();
    println!("Width: {} Height: {}", width, height);

    let distort_amount = 10;
    let x_freq = 2;
    let y_freq = 3;

    let n_frames = 240_i32;
    // How many frames for a full cycle of noise
    let frame_period = 120;
    let warmup_frames = 60;
    let mut frames: Vec<RgbaImage> = Vec::with_capacity(n_frames as usize);
    for frame_num in 0..n_frames {
        println!("Processing frame {}.", frame_num);
        let frame_proportion = (frame_num % frame_period) as f64 / frame_period as f64;
        let current_distort = if frame_num < warmup_frames {
            // Warm up
            distort_amount as f64 * (frame_num as f64 / warmup_frames as f64)
        } else if frame_num > (n_frames - warmup_frames) {
            // Slow down
            distort_amount as f64 * ((n_frames - frame_num) as f64 / warmup_frames as f64)
        } else {
            distort_amount as f64
        };
        let warped_frame = warp_image(&img, x_freq, y_freq, current_distort, frame_proportion);
        frames.push(warped_frame);
    }

    println!("Encoding to webp");
    let mut options = EncoderOptions::default();
    let config = EncodingConfig::new_lossy(50_f32);
    options.minimize_size = true;
    let mut encoder = Encoder::new_with_options((width, height), options).unwrap();
    encoder.set_default_encoding_config(config).unwrap();
    frames.iter().enumerate().for_each(|(frame_num, frame)| {
        let ms_per_frame = 1000_f64 / 30_f64;
        let timestamp = (frame_num + 1) as f64 * ms_per_frame;
        let frame_vec = frame.clone().into_vec();
        encoder
            .add_frame(&frame_vec, timestamp.round() as i32)
            .unwrap();
    });

    let final_timestamp = (n_frames / 30) * 1000;
    let webp_data = encoder.finalize(final_timestamp).unwrap();
    std::fs::write("my_animation.webp", webp_data).unwrap();
}

fn warp_image(
    img: &RgbaImage,
    x_freq: u32,
    y_freq: u32,
    distort_amount: f64,
    warp_offset: f64,
) -> RgbaImage {
    let perlin = Perlin::new();
    let x_period = img.width() / x_freq;
    let y_period = img.height() / y_freq;
    let mut warped_frame = RgbaImage::new(img.width(), img.height());
    for x in 0..img.width() {
        for y in 0..img.height() {
            let x_proportion: f64 = (x % x_period) as f64 / x_period as f64;
            let x_amount = ((x_proportion + warp_offset) * 2_f64 * PI).sin();
            let y_proportion: f64 = (y % y_period) as f64 / y_period as f64;
            let y_amount = ((y_proportion + warp_offset) * 2_f64 * PI).sin();
            let noise = perlin.get([x_amount, y_amount]);
            let mut distorted_x = ((x as f64) + (noise * distort_amount)).round();
            if distorted_x >= img.width() as f64 {
                distorted_x = (img.width() - 1) as f64;
            }
            let mut distorted_y = ((y as f64) + (noise * distort_amount)).round();
            if distorted_y >= img.height() as f64 {
                distorted_y = (img.height() - 1) as f64;
            }
            let pixel = img.get_pixel(distorted_x as u32, distorted_y as u32);
            warped_frame.put_pixel(x, y, pixel.clone());
        }
    }
    warped_frame
}

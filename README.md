# Perlin noise distortion in Rust

![Bismuth crystal](https://gitlab.com/warsquid/rust_warp/-/raw/main/bismuth.jpg)

![Animated Perlin distortion](https://gitlab.com/warsquid/rust_warp/-/raw/main/warp.webp)

*Image credit*: [Bismuth crystal](https://www.flickr.com/photos/paulslab/20138638353/) by [Paul](https://www.flickr.com/photos/paulslab/), [CC BY-NC-SA 2.0](https://creativecommons.org/licenses/by-nc-sa/2.0/)